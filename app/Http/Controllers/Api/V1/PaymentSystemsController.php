<?php


namespace App\Http\Controllers\Api\V1;

use App\Models\PaymentSystem;

class PaymentSystemsController
{
    /**
     * Get all payment systems details
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $paymentSystems = PaymentSystem::query()
            ->with('currencies')
            ->get();

        $return = [];
        /** @var PaymentSystem $paymentSystem */
        foreach ($paymentSystems as $paymentSystem) {
            $balances = [];
            foreach ($paymentSystem->currencies as $currency) {
                try {
                    $balances[$currency->code] = $paymentSystem->getClassInstance()::getBalance($currency->code);
                } catch (\Throwable $exception) {
                    $balances[$currency->code] = 0;
                }
            }
            $return[$paymentSystem->code] = [
                'name' => $paymentSystem->name,
                'external_balances' => $balances,
                'minimum_topup' => $paymentSystem->minimum_topup
                    ? json_decode($paymentSystem->minimum_topup, true) : [],
                'minimum_withdraw' => $paymentSystem->minimum_withdraw
                    ? json_decode($paymentSystem->minimum_withdraw, true) : [],
                'connected' => $paymentSystem->connected
            ];
        }

        return response()->json([
            'data' => $return
        ]);
    }
}
