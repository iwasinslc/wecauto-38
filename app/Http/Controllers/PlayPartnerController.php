<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Validator;

class PlayPartnerController extends Controller
{
    /**
     * @param $partnerId
     * @param $cyberId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function index($partnerId, $cyberId)
    {
        $check['partner_id'] = trim($partnerId);
        $check['cyber_id'] = trim($cyberId);

        $validator = Validator::make($check, [
            'partner_id' => 'required|exists:users,my_id',
            'cyber_id' => 'required|unique:users',
        ]);

        if ($validator->fails()) {
            return redirect('/')->withErrors($validator);
        }

        setcookie("partner_id", $partnerId, time() + 2592000, '/'); // expire in 30 days
        setcookie("cyber_id", $cyberId, time() + 2592000, '/'); // expire in 30 days
        $this->cleanCache();

        return redirect(route('register'));
    }

    /**
     * @throws \Exception
     */
    private function cleanCache()
    {
        $cacheKeys = [
            'i.partnerInfoFromCookies.' . $_SERVER['REMOTE_ADDR'],
            'i.cyberIdInfoFromCookies.' . $_SERVER['REMOTE_ADDR'],
        ];

        clearCacheByArray($cacheKeys);
    }
}
