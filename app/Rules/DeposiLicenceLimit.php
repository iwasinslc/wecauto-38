<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

/**
 * Class RuleHasPhone
 * @package App\Rules
 */
class DeposiLicenceLimit implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $licence = user()->licence;

        $min = $licence->deposit_min;

        $max = $licence->deposit_max;

        return $min <= $value && $value <= $max;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The amount is not included in the license limitation');
    }
}
