<?php

namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 * @package App\Models
 *
 * @property integer id
 * @property string user_id
 * @property array data
 * @property string message
 * @property string type
 * @property User user
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Notification extends Model
{
    use ModelTrait;

    /** @var array $fillable */
    protected $fillable = [
        'user_id',
        'message',
        'data',
        'type'
    ];

    protected $casts = [
        'data' => 'array'
    ];

    protected $table = 'notifications';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function translate(User $user)
    {
        return __($this->message, (array)json_decode($this->data, true), $user->locale);
    }

    /**
     * @param User $user
     * @param string $message
     * @param null $data
     * @param string $type
     * @return mixed
     */
    public static function add(User $user, $message='', $data = null, $type='success')
    {
       return self::create([
            'user_id'=>$user->id,
            'message'=>$message,
            'data'=>json_encode($data),
            'type'=>$type
        ]);
    }

}
