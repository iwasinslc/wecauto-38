<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Licence",
 *     description="User's Licence",
 *     @OA\Xml(
 *         name="Licence"
 *     )
 * )
 */
class Licence
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="Licence ID",
     *     example=1
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *     title="Licence name",
     *     description="Name of the licence",
     *     example="Licence 1"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     title="Duration",
     *     description="Number of duration days for licence",
     *     example=365
     * )
     *
     * @var integer
     */
    public $duration;

    /**
     * @OA\Property(
     *     title="Sell Limit",
     *     description="Sell dayly limit set for current user license",
     *     example=1000
     * )
     *
     * @var integer
     */
    public $sell_limit;

    /**
     * @OA\Property(
     *     title="Buy Limit",
     *     description="Buy dayly limit set for current user license",
     *     example=10000
     * )
     *
     * @var integer
     */
    public $buy_limit;
}