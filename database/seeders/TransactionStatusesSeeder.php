<?php

namespace Database\Seeders;

use App\Models\TransactionStatus;
use Illuminate\Database\Seeder;

class TransactionStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $withdrawStatuses = [
            [
                'id' => TransactionStatus::STATUS_CREATED,
                'name' => 'Created'
            ], [
                'id' => TransactionStatus::STATUS_APPROVED,
                'name' => 'Approved by admin'
            ], [
                'id' => TransactionStatus::STATUS_CONFIRMED_BY_EMAIL,
                'name' => 'Confirmed by email'
            ], [
                'id' => TransactionStatus::STATUS_REJECTED,
                'name' => 'Rejected'
            ], [
                'id' => TransactionStatus::STATUS_ERROR,
                'name' => 'Error'
            ]
        ];

        foreach ($withdrawStatuses as $status) {
            $searchStatus = TransactionStatus::find($status['id']);

            if ($searchStatus != null) {
                echo "Withdraw status '". $status['name'] ."' already registered.\n";
                continue;
            }

            TransactionStatus::create([
                'id' => $status['id'],
                'name' => $status['name']
            ]);
            echo "Transaction type '".$status['name']."' registered.\n";
        }
    }
}
