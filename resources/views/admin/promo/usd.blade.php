@extends('admin/layouts.app')
@section('title')
    {{ __('Промо USD') }}
@endsection
@section('breadcrumbs')
    <li>   {{ __('Промо USD') }}</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <div class="col-md-12">
            <section class="tile tile-simple">
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">
                        {{ __('Промо USD') }}
                    </h1>

                </div>
                <div class="tile-body">
                    <table id="plans-wec" class="table hover form-inline dt-bootstrap no-footer">
                        <thead>
                        <tr>
                            <th>{{__('Login')}}</th>
                            <th>{{ __('Amount') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    @push('load-scripts')
                        <script>
                            $('#plans-wec').DataTable({
                                "iDisplayLength": 25,
                                "processing": true,
                                "serverSide": true,
                                "order": [[1, "desc"]],
                                "ajax": '{{ route('admin.promo.ddata_usd') }}',
                                "columns": [
                                    {
                                        "data": "user.login", "render": function (data, type, row, meta) {
                                            return '<a href="' + row['show'] + '" target="_blank">' + data + '</a>';
                                        }
                                    },

                                    {"data": "balance"},
                                ],
                                "aoColumnDefs": [
                                    {
                                        'bSortable': false,
                                        'aTargets': ["no-sort"],
                                        'render': function (data, type, row) {
                                            return data + ' (' + row[1] + ')';
                                        },
                                    }],
                                "aoColumnDefs": [
                                    {'bSortable': false, 'aTargets': ["no-sort"],}
                                ],
                                "dom": 'Rlfrtip',
                                initComplete: function () {
                                    this.api().columns([0,1]).every(function () {
                                        var column = this;
                                        var input = document.createElement("input");
                                        $(input).appendTo($(column.footer()).empty()).attr('placeholder', '{{ __('search ...') }}')
                                            .on('change', function () {
                                                column.search($(this).val(), false, false, true).draw();
                                            });
                                    });
                                }
                            });
                        </script>
                    @endpush
                </div>
            </section>
        </div>
    </div>
@endsection
