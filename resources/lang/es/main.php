<?php
return [
    'emails' => [
        'spam_protection'   => 'La carta no está enviada. Reglas del correo basura.',
        'sent_successfully' => 'La carta está enviada con éxito',
        'unable_to_send'    => 'Es imposible enviar esta carta',

        'request' => [
            'email_required' => 'Email es obligatorio',
            'email_max'      => 'La longitud máxima de email es de 255 símbolos',
            'email_email'    => '¡email incorrecto!',

            'text_required' => 'Texto es obligatorio',
            'text_min'      => 'La longitud mínima de texto es de 10 símbolos',
        ],
    ],
    'transaction_types' => [
        'enter'      => 'Recargo del saldo',
        'withdraw'   => 'Retiro de los fondos',
        'bonus'      => 'Bono',
        'partner'    => 'Comisión de socio',
        'dividend'   => 'Ganancia del depósito',
        'create_dep' => 'Creación del depósito',
        'close_dep'  => 'Cierre del depósito',
        'penalty'    => 'Multa',
    ],

];
